package thefrenchkiwi.tp1;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class CountryActivity extends AppCompatActivity {

    private Bundle bundleCountry;
    private Country country;
    private TextView title;
    private ImageView flag;
    private TextView capitale;
    private TextView langues;
    private TextView monnaie;
    private TextView pop;
    private TextView area;
    private TextView c;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);
        //Retrieval of data
        Intent intent = getIntent();
        bundleCountry = intent.getBundleExtra("countryData");

        //Finding layout attributes by id
        title = (TextView) findViewById(R.id.title);
        flag = (ImageView) findViewById(R.id.flag);
        capitale = (TextView) findViewById(R.id.capitaleInput);
        langues = (TextView) findViewById(R.id.languesInput);
        monnaie = (TextView) findViewById(R.id.monnaieInput);
        pop = (TextView) findViewById(R.id.popInput);
        area = (TextView) findViewById(R.id.superficieInput);

        country = CountryList.getCountry(bundleCountry.getString("country"));

        title.setText(bundleCountry.getString("country"));
        //Setting flag
        Context context = flag.getContext();
        int id = context.getResources().getIdentifier(country.getmImgFile(), "drawable", context.getPackageName());
        flag.setImageResource(id);
        capitale.setText(country.getmCapital());
        langues.setText(country.getmLanguage());
        monnaie.setText(country.getmCurrency());
        pop.setText(Integer.toString(country.getmPopulation()));
        area.setText(Integer.toString(country.getmArea()));
        //area.setText(Integer.toString(bundleCountry.getInt("superficie")) + "km2");

        Button saveButton = findViewById(R.id.button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                country.setmCapital(capitale.getText().toString());
                country.setmLanguage(langues.getText().toString());
                country.setmCurrency(monnaie.getText().toString());
                country.setmPopulation(Integer.parseInt(pop.getText().toString()));
                country.setmArea(Integer.parseInt(area.getText().toString()));

                CountryList.saveCountry(bundleCountry.getString("country"),country);
                Toast.makeText(CountryActivity.this, "Sauvegardé!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(CountryActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

}

