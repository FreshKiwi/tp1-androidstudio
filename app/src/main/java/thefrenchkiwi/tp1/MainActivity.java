package thefrenchkiwi.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{

    private ListView mListView;
    private CountryList myCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mListView = (ListView) findViewById(R.id.listView);
        String [] values = myCountry.getNameArray();

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,
                android.R.layout.simple_list_item_1,values);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView parent, View v, int position, long id)
                    {
                        // Do something in response to the click
                        final String item = (String) parent.getItemAtPosition(position);
                        Toast.makeText(MainActivity.this, "You selected: " + item, Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(MainActivity.this, CountryActivity.class);
                        Country selectedCountry = CountryList.getCountry(item);
                        Bundle bundle = new Bundle();
                        bundle.putString("country", item);
                        /*
                        bundle.putString("capitale",selectedCountry.getmCapital());
                        bundle.putString("flag",selectedCountry.getmImgFile());
                        bundle.putString("langue",selectedCountry.getmLanguage());
                        bundle.putString("monnaie", selectedCountry.getmCurrency());
                        bundle.putInt("population", selectedCountry.getmPopulation());
                        bundle.putInt("superficie", selectedCountry.getmArea());
                        */
                        intent.putExtra("countryData", bundle);
                        startActivity(intent);
                    }
                });
    }
}
